public class Player {
    String playerName;
    int playerNumber;
    String playerGender;

    public Player(String name, int number, String gender) {
        playerName = name;
        playerNumber = number;
        playerGender = gender;
    }

    public void playersGreeting() {
        String num = String.format("%03d", playerNumber);
        System.out.println("Hello, " + playerName + "!\nWelcome to the game!\nYour number is " + num + ".");
    }



    public static void main(String[] args) {
        Player IlNam = new Player ("Oh Il-Nam", 003, "male");
        Player SangWoo = new Player ("Cho Sang-Woo", 218, "male");
        Player Kang = new Player ("Kang Sae-Byeok", 067, "female");



        IlNam.playersGreeting();


    }
}



